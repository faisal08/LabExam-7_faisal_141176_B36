$(document).ready(function() {

    // Generate a simple captcha
    $('#summaryform').bootstrapValidator({

        message: 'This value is not valid',

        fields: {
           org_name: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The organization name is required and can\'t be empty'
                    }
                }
            },
            org_summary: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The summary is required and can\'t be empty'
                    }
                }
            }
        }
    });
});